<?php

namespace App\Service;

/**
 * Description of ContactUsResult - it gives back the result of the saved data
 */
class ContactUsResult {
    
    private $name;
    private $email;
    
    public function __construct($name, $email) 
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail() 
    {
        return $this->email;
    }

}
