<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\ContactUs;
use App\Repository\ContactUsRepository;
/**
 * Description of ContactUsService
 *
 * @author Csabarnus
 */
class ContactUsService {
    
    private $contactUsRepository;
    
    public function __construct( ContactUsRepository $contactUsRepository) 
    {
        $this->contactUsRepository = $contactUsRepository;
    }

    public function create( ContactUs $contactData )
    {
        $this->contactUsRepository->saveContacting($contactData);
        
        return new ContactUsResult($contactData->getName(), $contactData->getEmail() );
    }
    
}
