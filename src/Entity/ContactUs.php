<?php
declare(strict_types = 1);

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ContactUsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContactUsRepository::class)
 * @ORM\Table(name="contacting")
 */
class ContactUs
{   
    /**
     * 
     * @var type
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="bigint")
     */
    private $id;
    
    /**
     * @var type
     * @Assert\NotBlank( message = "A '{{ field }}' kitöltése szükséges." )
     * @Assert\Length( min = 3, max = 50,
     *          minMessage = "Az név min.{{ limit }} karakter hosszúságúnak kell lennie.",
     *          maxMessage = "Az név max.{{ limit }} karakter hosszúságú lehet."
     *      )
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    
    /**
     * 
     * @var type
     * @Assert\NotBlank( message = "A '{{ field }}' kitöltése szükséges." )
     * @Assert\Email( message = "A megadott '{{ value }}' email cím nem hiteles." )
     * @Assert\Length( min = 10, max = 200,
     *          minMessage = "Az email címnek min.{{ limit }} karakter hosszúságúnak kell lennie.",
     *          maxMessage = "Az email címnek max.{{ limit }} karakter hosszúságú lehet."
     *      )
     * @ORM\Column(type="string", length=150)
     */
    private $email;
    
    /**
     * 
     * @var type
     * @Assert\NotBlank( message = "A '{{ field }}' kitöltése szükséges." )
     * @Assert\Length( min = 10, max = 500,
     *          minMessage = "Az üzenetnek min.{{ limit }} karakter hosszúságúnak kell lennie.",
     *          maxMessage = "Az üzenetnek max.{{ limit }} karakter hosszúságú lehet."
     *      )
     * @ORM\Column(type="text")
     */
    private $message;
    
    
    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setEmail(string $email): void 
    {
        $this->email = $email;
    }

    public function setMessage(string $message): void 
    {
        $this->message = $message;
    }
}