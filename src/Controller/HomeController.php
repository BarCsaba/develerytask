<?php
//declare(strict_types = 1);
namespace App\Controller;

use App\Form\ContactUsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", "/home", name="home", methods={"GET"})
     */
    public function index(): Response
    {
        $form = $this->createForm( ContactUsType::class);
        
        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
