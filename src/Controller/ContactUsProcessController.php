<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Entity\ContactUs;
use App\Form\ContactUsType;
use App\Service\ContactUsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ContactUsProcessController extends AbstractController
{
    private $contactUsService;
    
    public function __construct( ContactUsService $contactUsService)
    {
        $this->contactUsService = $contactUsService;
    }

    /**
     * @Route("/", name="contact_us_process", methods={"POST"})
     */
    public function index( Request $request): Response
    {
        $form = $this->createForm( ContactUsType::class, (new ContactUs()) );
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
            $contactUsFormData = $form->getData();
            
            $contactUsResult = $this->contactUsService->create( $contactUsFormData);
            
            $this->addFlash('senderName', $contactUsResult->getName());
            $this->addFlash('senderEmail', $contactUsResult->getEmail());
            
            return $this->redirectToRoute('home');
        }
        else
        {
            return $this->render('home/index.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }
        
}
