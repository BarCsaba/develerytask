<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Types\Types;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210519161437 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('contacting');
        $table->addColumn('id', Types::BIGINT, ['autoincrement' => true]);
        $table->addColumn('name', Types::STRING, ['length' => 50]);
        $table->addColumn('email', Types::STRING, ['length' => 150]);
        $table->addColumn('message', Types::TEXT);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('contacting');
    }
}